# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python

'''
This is the startup script. Main class initializes the programs
and listens for input.

'''

# setup empty folders if they do not already exist
# this code needs to be processed before the other application pkg import statements
import os, traceback
import configparser
from framework import Constants

config = configparser.ConfigParser()
config.readfp(open(Constants.FILE_CONF))

try:
    if not os.path.exists(config.get('Logger', 'dir_log')):
        os.makedirs(config.get('Logger', 'dir_log'))
        print ('Created directory: ' + config.get('Logger', 'dir_log'))
        
    if not os.path.exists(config.get('Camera', 'dir_stills')):
        os.makedirs(config.get('Camera', 'dir_stills'))
        print ('Created directory: ' + config.get('Camera', 'dir_stills'))
        
    if not os.path.exists(config.get('Camera', 'dir_videos')):
        os.makedirs(config.get('Camera', 'dir_videos'))
        print ('Created directory: ' + config.get('Camera', 'dir_videos'))
        
    if not os.path.exists(config.get('Camera', 'dir_sequences')):
        os.makedirs(config.get('Camera', 'dir_sequences'))
        print ('Created directory: ' + config.get('Camera', 'dir_sequences'))
except:
    traceback.print_exc()


from time import sleep
from Processor import Processor

from framework.Logger import Logger

class Main():

    def main():
        myLogger = Logger().getLogger()

        # startup log
        with open(config.get('Logger','file_log_startup'), 'r') as fin:
            startupText = fin.read()
            myLogger.info("Launching 'Security without Subscription'" + startupText)


        # Initialize
        processor = Processor()

        # Notify user with a bell
        os.system('mpg123 ' + config.get('Audio', 'file_bell'))

        myLogger.info('Listening for input')
        while True:
            processor.main()
            sleep(0.1)



    if __name__ == "__main__":
        main()
    

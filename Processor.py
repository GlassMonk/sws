# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python

'''
This efines all the processing code and is called as needed from Main.py.
Should not be invoked directly.
'''
from gpiozero import Button, LED
from time import sleep
import time, os, subprocess, configparser, threading

from framework import Constants
from framework.Logger import Logger
from helpers.MotionHelper import MotionHelper
from helpers.CameraHelper import CameraHelper
from helpers.GoogleDriveHelper import GoogleDriveHelper
from helpers.InstapushHelper import InstapushHelper
from helpers.EmailHelper import EmailHelper

class Processor():

    def __init__(self):

        self.config = configparser.ConfigParser()
        self.config.readfp(open(Constants.FILE_CONF))

        self.btnBell = Button(self.config.getint('Ports', 'btn_bell'))
        self.led = LED(self.config.getint('Ports', 'led_motion_sensor'))
        
        self.myLogger = Logger().getLogger()
        self.motionHelper = MotionHelper()
        self.cameraHelper = CameraHelper()
        self.emailHelper = EmailHelper()
        self.googleDriveHelper = GoogleDriveHelper()
        self.instapushHelper = InstapushHelper()
        
        # Simplistic lock that ensures multiple requests to capture image and
        # video are not received when the camera is already processing one request
        self.cameraThreadLock = threading.Lock()

        # Simplistic lock that ensures multiple requests are not sent to the motion detector
        self.motionThreadLock = threading.Lock()



    # Plays the audio file defined in main.cfg
    def __ringBell(self):
        self.myLogger.info('Ringing the bell audio')
        os.system('mpg123 ' + self.config.get('Audio', 'file_bell') + ' &')



    # Triggers the camera module to capture image and video
    # Calls the notifyDoorbellEvents method internally to send out notifications
    def __processDoorbell(self):

        self.myLogger.debug('Entering __processDoorbell()')

        try:
            
            self.cameraThreadLock.acquire()

            # Capture image and video
            imageFile = self.cameraHelper.captureImage()
            videoFile = self.cameraHelper.captureVideo()

            # Spawn a daemon to handle notification so the camera is not held up
            doorbellNotificationThread = threading.Thread(name='Doorbell Notifier Thread', target=self.__notifyDoorbellEvents, args=[imageFile,videoFile])
            doorbellNotificationThread.setDaemon(True)
            doorbellNotificationThread.start()

        except:
            # This is not fatal for the rest of the program. Log error and continue.
            self.myLogger.exception('Unable to process Doorbell')
            pass

        finally:
            self.cameraThreadLock.release()
        
        self.myLogger.debug('Exiting __processDoorbell()')



    # Sends the email notification, if enabled; Uploads the files to Google Drive, if enabled
    # and pushes notifications to Instapush, if enabled
    def __notifyDoorbellEvents(self, imageFile, videoFile):

        self.myLogger.debug('Entering __notifyDoorbellEvents(...)')

        try:
            # Send an email with the attachments
            if self.config.getboolean('Mail', 'is_enabled'):
                self.emailHelper.sendDoorbellMail([imageFile, videoFile])

            # Upload to Google Drive        
            if self.config.getboolean('GoogleDrive', 'is_enabled'):
                imageURL = self.googleDriveHelper.uploadImageAsset(imageFile)
                videoURL = self.googleDriveHelper.uploadVideoAsset(videoFile)

                # Send notification with links to Instapush
                if self.config.getboolean('Instapush', 'is_enabled'):
                    self.instapushHelper.pushStillMessage(imageURL)
                    self.instapushHelper.pushVideoMessage(videoURL)

        except:
            # This is not fatal for the rest of the program. Log error and continue.
            self.myLogger.exception('Unable to notify doorbell events')
            pass

        self.myLogger.debug('Exiting __notifyDoorbellEvents(...)')


    # Triggers the camera module to capture image 
    # Calls the notifyMotionEvents method internally to send out notifications
    def __processMotion(self):

        self.myLogger.debug('Entering __processMotion()')

        motionTriggerDelay = self.config.getint('MotionSensor', 'trigger_delay')
        ledOffTimer = self.config.getint('MotionSensor', 'led_delay')

        try:
            # Acquire lock so that addtional motion detection daemons are not spawned
            self.motionThreadLock.acquire()
            # Indicate the sensor is active
            self.led.blink()
            sleep(3)
            self.led.off()

            # Wait for motion
            if self.motionHelper.detectMotion():

                # Turn LED on to indicate motion
                self.led.on()
            
                # Capture image sequence if the camera is available
                if self.cameraThreadLock.locked():
                    self.myLogger.info('Camera is busy. Skipping image sequence capture')
                else:
                    self.cameraThreadLock.acquire()
                    imageFileList = self.cameraHelper.captureImageSequence()
                    self.cameraThreadLock.release()

                    # Spawn a daemon to handle notification so the motion thread is not held up
                    motionNotificationThread = threading.Thread(name='Motion Notifier Thread', target=self.__notifyMotionEvents, args=[imageFileList])
                    motionNotificationThread.setDaemon(True)
                    motionNotificationThread.start()

            # If led is configured to turned off before motion sensor reactivation
            if ledOffTimer < motionTriggerDelay:
                # turn off led
                sleep(ledOffTimer)
                self.led.off()

                # Delay subsequent motion triggers by holding on to the lock
                sleep(motionTriggerDelay-ledOffTimer)

            else:
                sleep(motionTriggerDelay)
                

        except:
            # This is not fatal for the rest of the program. Log error and continue.
            self.myLogger.exception('Unable to process Motion')
            pass

        finally:
            self.led.off()
            self.motionThreadLock.release()
        
        self.myLogger.debug('Exiting __processMotion()')



    # Sends the email notification, if enabled; Uploads the files to Google Drive, if enabled
    # and pushes notifications to Instapush, if enabled
    def __notifyMotionEvents(self, imageFileList):

        self.myLogger.debug('Entering __notifyMotionEvents(...)')

        try:
            # Send an email with the attachments
            if self.config.getboolean('Mail', 'is_enabled'):
                self.emailHelper.sendMotionMail(imageFileList)

            # Upload to Google Drive        
            if self.config.getboolean('GoogleDrive', 'is_enabled'):
                imageURLList = self.googleDriveHelper.uploadSequence(imageFileList)

                # Send notification with links to Instapush
                if self.config.getboolean('Instapush', 'is_enabled'):
                    self.instapushHelper.pushSequenceMessage(imageURLList)
        except:
            # This is not fatal for the rest of the program. Log error and continue.
            self.myLogger.exception('Unable to notify motion events')
            pass

        self.myLogger.debug('Exiting __notifyMotionEvents(...)')

        

    # Main method that handles the processing. All other methods in this class are internal helper methods
    def main(self):

        if (self.btnBell.is_pressed):
            self.myLogger.info('Button pressed')

            self.__ringBell()

            # Spawn a daemon to handle image and video so the main
            # thread that handles the bell is not held up
            # Make sure only one daemon is active at a given point in time
            if self.cameraThreadLock.locked():
                self.myLogger.info('Camera is busy. Skipping image and video processing')
            else:
                cameraThread = threading.Thread(name='Doorbell Thread',target=self.__processDoorbell)
                cameraThread.setDaemon(True)
                cameraThread.start()

        # Spawn a daemon to handle motion detection
        # Make sure only one daemon is active at a given point in time
        if self.motionThreadLock.locked() == False:
            motionThread = threading.Thread(name='Motion Thread', target=self.__processMotion)
            motionThread.setDaemon(True)
            motionThread.start()
            


    

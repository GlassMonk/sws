# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python
'''
simple logger wrapper
'''
import logging, configparser

from framework import Constants

class Logger():

    config = configparser.ConfigParser()
    config.readfp(open(Constants.FILE_CONF))

    # create logger
    logger = logging.getLogger('SwS')
    logger.setLevel(config.getint('Logger', 'log_level_file'))

    # create file handler
    fh = logging.FileHandler(config.get('Logger', 'file_log'))
    fh.setLevel(config.getint('Logger', 'log_level_file'))

    # create console handler 
    ch = logging.StreamHandler()
    ch.setLevel(config.getint('Logger', 'log_level_console'))

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - [%(threadName)s: %(thread)d] - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    def getLogger(self):
        return self.logger


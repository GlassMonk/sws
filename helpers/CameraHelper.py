# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python
'''
Helper class. Includes everthing that is needed to control the camera
Ref: https://picamera.readthedocs.io/en/release-1.12/recipes1.html
'''
from picamera import PiCamera
from time import sleep
import time, configparser

from framework import Constants
from framework.Logger import Logger

class CameraHelper():

    def __init__(self):
        self.myLogger = Logger().getLogger()
        self.myLogger.debug('Initializing CameraHelper')

        self.config = configparser.ConfigParser()
        self.config.readfp(open(Constants.FILE_CONF))

    # Rotates the camera so the image is right side up
    # Reads input from config
    def __rotateCamera(self):
        rotationDict = {'power_up':-90,'power_down':90,'power_right':-180, 'power_left':0}
        return rotationDict.get(self.config.get('Camera', 'orientation'))
        

    # Tests the camera by lanuching preview. No images are captured
    def testCamera(self):
        self.myLogger.debug('Entering testCamera()')
        
        camera = PiCamera()
        try:
            camera.resolution = (1280,720)
            camera.rotation = self.__rotateCamera() #orients the camera
            camera.start_preview()
            sleep(2) #giving it time to adjust to the ambient light conditions
            camera.stop_preview()
        except:
            # This is fatal for this thread. Let the exception propagate.
            self.myLogger.exception('Unable to test camera')
        finally:
            camera.close()

        self.myLogger.debug('Exiting testCamera()')


    # Saves image to the drive and returns the file path
    def captureImage(self):
        self.myLogger.debug('Entering captureImage()')
        
        camera = PiCamera()
        file = None

        try:
            camera.resolution = (1280,720)
            camera.rotation = self.__rotateCamera() #orients the camera
            camera.start_preview()
            sleep(2) #giving it time to adjust to the ambient light conditions
            file = self.config.get('Camera', 'dir_stills') + str(time.time()) + ".jpg"
            camera.capture(file)
            camera.stop_preview()
            self.myLogger.info('Captured image: ' + file)
        except:
            # This is fatal for this thread. Let the exception propagate.
            self.myLogger.exception('Unable to capture image')
        finally:
            camera.close()

        self.myLogger.debug('Exiting captureImage()')
        return file

    # Captures a video to the drive and returns the file path
    # Video length is defined in main.cfg
    def captureVideo(self):
        self.myLogger.debug('Entering captureVideo()')
        
        camera = PiCamera()
        file = None

        try:
            camera.resolution = (640,360)
            camera.framerate = 17
            camera.rotation = self.__rotateCamera() #orients the camera
            file = self.config.get('Camera', 'dir_videos') + str(time.time()) + '.mp4'
            camera.start_recording(file,'h264')
            camera.wait_recording()
            sleep(self.config.getint('Camera', 'video_duration'))
            camera.stop_recording()
            self.myLogger.info('Captured video: ' + file)
        except:
            # This is fatal for this thread. Let the exception propagate.
            self.myLogger.exception('Unable to capture video')
        finally:
            camera.close()

        self.myLogger.debug('Exiting captureVideo()')
        return file


    # Saves image sequence to the drive and returns the file path list
    # Number of images and the delay in between are defined in main.cfg
    def captureImageSequence(self):
        self.myLogger.debug('Entering captureImageSequence()')
        
        camera = PiCamera()
        fileList = []

        try:
            camera.resolution = (640,360)
            camera.rotation = self.__rotateCamera() #orients the camera
            camera.start_preview()
            sleep(2) #giving it time to adjust to the ambient light conditions

            filename_prefix = self.config.get('Camera', 'dir_sequences') + str(time.time())

            # Capture image sequence
            counter = 0
            # camera.capture_continuous() seems to work only with the for statement
            for filename in camera.capture_continuous(filename_prefix + '-{counter:03d}.jpg'):
                self.myLogger.info('Captured image: ' + filename)

                fileList.append(filename)
                counter += 1
                if counter >= self.config.getint('Camera', 'sequence_limit'):
                    break
                sleep (self.config.getint('Camera', 'sequence_delay'))

            camera.stop_preview()

            self.myLogger.info('Captured image sequence: ' + str(fileList))

        except:
            # This is fatal for this thread. Let the exception propagate.
            self.myLogger.exception('Unable to capture image sequence')
        finally:
            camera.close()

        self.myLogger.debug('Exiting captureImageSequence()')
        return fileList

# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python
'''
Helper class. Includes everthing that is needed to send email via smtp
Ref: https://docs.python.org/2/library/smtplib.html
'''
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib, configparser

from framework import Constants
from framework.Logger import Logger

class EmailHelper():
    def __init__(self):
        self.myLogger = Logger().getLogger()
        self.myLogger.debug('Initializing EmailHelper...')

        self.config = configparser.ConfigParser()
        self.config.readfp(open(Constants.FILE_CONF))
        
        self.smtp = self.config.get('Mail', 'smtp')
        self.fromAddress = self.config.get('Mail', 'from_address')
        self.password = self.config.get('Mail', 'password')
        self.fromName = self.config.get('Mail', 'name')
        self.toAddress = self.config.get('Mail', 'to_address')        


    # Sends an email with optional attachments and with the doorbell event subject and body.
    # Attachments can be specified as comma separated file paths enclosed within square brackets
    # e.g., ['/home/pi/1483225173.9436545.jpg', '/home/pi/1483227007.4328618.h264']
    # Internally uses the __sendMail(...) method
    def sendDoorbellMail(self, files=None):
        self.myLogger.debug('Entering sendDoorbellMail(...)')
        
        subject = self.config.get('Mail', 'doorbell_subject')
        message = self.config.get('Mail', 'doorbell_message')
        self.__sendMail(subject, message, files)

        self.myLogger.debug('Exiting sendDoorbellMail(...)')

    # Sends an email with optional attachments and with the motion event subject and body.
    # Attachments can be specified as comma separated file paths enclosed within square brackets
    # e.g., ['/home/pi/1483225173.9436545.jpg', '/home/pi/1483227007.4328618.h264']
    # Internally uses the __sendMail(...) method
    def sendMotionMail(self, files=None):
        self.myLogger.debug('Entering sendMotionMail(...)')
        
        subject = self.config.get('Mail', 'motion_subject')
        message = self.config.get('Mail', 'motion_message')
        self.__sendMail(subject, message, files)

        self.myLogger.debug('Exiting sendMotionMail(...)')



    # Intended to be a private helper method
    # Sends an email with optional attachments. Attachments can be specified as
    # comma separated file paths enclosed within square brackets
    # e.g., ['/home/pi/1483225173.9436545.jpg', '/home/pi/1483227007.4328618.h264']
    def __sendMail(self, subject, message, files=None):
        self.myLogger.debug('Entering sendMail(...)')

        # Build mail header
        msg = MIMEMultipart()
        msg['From'] = self.fromName + '<' + self.fromAddress + '>'
        msg['To'] = self.toAddress
        msg['Subject'] = subject

        # Add mail body content
        msg.attach(MIMEText(message))

        # Add attachments
        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
                part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
                msg.attach(part)

        # Connect to server and send mail
        try:
            server = smtplib.SMTP(self.smtp)
            server.starttls()
            server.login(self.fromAddress, self.password)
            server.sendmail(self.fromAddress, self.toAddress, str(msg))
            server.quit()
            self.myLogger.info('Sent Email to: ' + self.toAddress)
        except:
            # This is not fatal for the rest of the program. Log error and continue
            self.myLogger.exception('Unable to send email')
            pass
        
        self.myLogger.debug('Exiting sendMail(...)')

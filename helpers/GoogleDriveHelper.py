# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python
'''
Helper class. Includes everthing that is needed to connect and upload to
Google Drive
Ref https://developers.google.com/drive/v3/web/quickstart/python
'''
from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from googleapiclient.http import MediaFileUpload

from framework import Constants
from framework.Logger import Logger
import configparser

class GoogleDriveHelper():

    def __init__(self):
        self.myLogger = Logger().getLogger()
        self.myLogger.debug('Initializing GoogleDriveHelper...')
    
        self.config = configparser.ConfigParser()
        self.config.readfp(open(Constants.FILE_CONF))

        # Asset source folders
        self.dir_stills = self.config.get('GoogleDrive', 'dir_stills')
        self.dir_videos = self.config.get('GoogleDrive', 'dir_videos')
        self.dir_sequences = self.config.get('GoogleDrive', 'dir_sequences')

        # Delete after upload?
        self.cleanup_stills = self.config.getboolean('GoogleDrive', 'still_delete_after_upload')
        self.cleanup_videos = self.config.getboolean('GoogleDrive', 'video_delete_after_upload')
        self.cleanup_sequences = self.config.getboolean('GoogleDrive', 'sequence_delete_after_upload')

        
    # Uploads the image to the appropriate folder on Google Drive and returns the direct access URL to the image
    def uploadImageAsset(self, imageFile):
        self.myLogger.debug('Entering uploadImageAsset(...)')
        
        imageURL = self.__uploadAsset(imageFile,self.dir_stills, self.cleanup_stills)

        self.myLogger.debug('Exiting uploadImageAsset()')
        return imageURL
    
    # Uploads the video to the appropriate folder on Google Drive and returns the direct access URL to the video
    def uploadVideoAsset(self, videoFile):
        self.myLogger.debug('Entering uploadVideoAsset(...)')

        videoURL = self.__uploadAsset(videoFile,self.dir_videos, self.cleanup_videos)

        self.myLogger.debug('Exiting uploadVideoAsset()')
        return videoURL

    # Uploads the image sequence to the appropriate folder on Google Drive and returns the list of
    # direct access URL to the images
    def uploadSequence(self, imageFileList):
        self.myLogger.debug('Entering uploadSequence(...)')

        imageURLList = []
        for imageFile in imageFileList or []:
            imageURLList.append(self.__uploadAsset(imageFile,self.dir_sequences, self.cleanup_sequences))

        self.myLogger.info('Image sequence URL list: ' + str(imageURLList))
        self.myLogger.debug('Exiting uploadSequence()')
        return imageURLList


    # Private helper method. Handles authentication and returns service 
    def __getService(self):
        self.myLogger.debug('Entering getService()')

        try:
            import argparse
            flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
        except ImportError:
            flags = None

        # If modifying these scopes, delete the previously saved credentials
        scopes = 'https://www.googleapis.com/auth/drive'
        client_scret_file = self.config.get('GoogleDrive', 'file_secret')
        application_name = self.config.get('GoogleDrive', 'application_name')

        #Gets valid user credentials from storage.
        #If nothing has been stored, or if the stored credentials are invalid,
        #the OAuth2 flow is completed to obtain the new credentials.
        credential_path = self.config.get('GoogleDrive', 'file_credentials')
        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(client_scret_file, scopes)
            flow.user_agent = application_name
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)

            self.myLogger.debug('Storing credentials to ' + credential_path)

        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v3', http=http)

        self.myLogger.debug('Exiting getService()')
        return service



    # Private helper method. Uploads asset to Google Drive.
    # Parameters:
    #   file_source: Local path of the source file
    #   dir_destination: Google drive asset id of the destination folder
    #   cleanup: If set to 'True', the local copy will be deleted after upload 
    def __uploadAsset(self, file_source, dir_destination, cleanup):
        
        self.myLogger.debug('Entering uploadAsset(...)')
        link = None

        try:
            driveService = self.__getService()
            media = MediaFileUpload(file_source)
            response = driveService.files().create(media_body=media,
                            body={'name':os.path.basename(file_source),
                                  'parents':[dir_destination]}).execute()
            
            link = self.config.get('GoogleDrive', 'url_file_access') + response['id']

            self.myLogger.info('Uploaded asset metadata: ' + str(response))
            self.myLogger.info('Uploaded asset access URL: ' + link)

            if cleanup:
                os.remove(file_source)
                self.myLogger.info('Deleted asset: ' + file_source)

        except:
            # This is not fatal for the rest of the program. Log error and continue.
            self.myLogger.exception('Unable to upload to Google Drive')
            pass

        self.myLogger.debug('Exiting uploadAsset(...)')
        return link
'''
#Unit testing code
config = configparser.ConfigParser()
config.readfp(open(Constants.FILE_CONF))
googleDriveHelper = GoogleDriveHelper()
dir_stills = config.get('GoogleDrive', 'dir_stills')
dir_videos = config.get('GoogleDrive', 'dir_videos')
cleanup_stills = config.getboolean('GoogleDrive', 'still_delete_after_upload')
cleanup_videos = config.getboolean('GoogleDrive', 'video_delete_after_upload')
googleDriveHelper.uploadAsset('/home/pi/Documents/kartik/doorbell/stills/1483204546.1476433.jpg',dir_stills, cleanup_stills)
googleDriveHelper.uploadAsset('/home/pi/Documents/kartik/doorbell/videos/1483205039.7001677.h264',dir_videos, cleanup_videos)
'''

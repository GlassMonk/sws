# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python
'''
Helper class. Includes everthing that is needed to connect to Instapush and
send notifications
Ref: Instapush source code https://pypi.python.org/pypi/instapush 
'''
import configparser

from instapush import Instapush, App

from framework import Constants
from framework.Logger import Logger

class InstapushHelper():
    def __init__(self):
        self.myLogger = Logger().getLogger()
        self.myLogger.debug('Initializing InstapushHelper...')

        self.config = configparser.ConfigParser()
        self.config.readfp(open(Constants.FILE_CONF))
        
        self.app_id = self.config.get('Instapush', 'app_id')
        self.app_secret = self.config.get('Instapush', 'app_secret')

        # Instapush event names
        self.still_event_name = self.config.get('Instapush', 'still_event_name')
        self.video_event_name = self.config.get('Instapush', 'video_event_name')
        self.sequence_event_name = self.config.get('Instapush', 'sequence_event_name')

        # Instapush tracker names
        self.still_tracker_name = self.config.get('Instapush', 'still_tracker_name')
        self.video_tracker_name = self.config.get('Instapush', 'video_tracker_name')
        self.sequence_tracker_prefix = self.config.get('Instapush', 'sequence_tracker_prefix')


    # Sends a doorbell image notification to the Instapush app
    # Recepients can click the link in the notification to view the image
    # Accepts the image URL as the parameter
    def pushStillMessage(self, imageURL):
        self.myLogger.debug('Entering pushStillMessage(...)')
        self.__pushMessage(self.still_event_name, {self.still_tracker_name:imageURL})
        self.myLogger.debug('Exiting pushStillMessage(...)')

    # Sends a doorbell video notification to the Instapush app
    # Recepients can click the link in the notification to view the video
    # Accepts the video URL as the parameter
    def pushVideoMessage(self, videoURL):
        self.myLogger.debug('Entering pushVideoMessage(...)')
        self.__pushMessage(self.video_event_name, {self.video_tracker_name:videoURL})
        self.myLogger.debug('Exiting pushVideoMessage(...)')

    # Sends a motion sequence notification to the Instapush app
    # Recepients can click the links in the notification to view the images
    # Accepts the image URL list as the parameter
    def pushSequenceMessage(self, imageURLList):
        self.myLogger.debug('Entering pushSequenceMessage(...)')

        # Build tracker dictionary from the image URL list
        trackers = {}
        for i, imageURL in enumerate(imageURLList):
            trackers.update({self.sequence_tracker_prefix + str(i+1) : imageURL})

        self.__pushMessage(self.sequence_event_name, trackers)
        self.myLogger.debug('Exiting pushVideoMessage(...)')


    # Private method that sends a notification to the Instapush app
    def __pushMessage(self, eventName, trackers):
        self.myLogger.debug('Entering pushMessage(...)')

        try:
            app = App(self.app_id, self.app_secret)
            app.notify(eventName, trackers)
            self.myLogger.info('Sent Instapush notification: ' + str(trackers))
        except:
            # This is not fatal for the rest of the program. Log error and continue.
            self.myLogger.exception('Unable to send an Instapush message')
            pass

        self.myLogger.debug('Exiting pushMessage(...)')

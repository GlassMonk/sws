# Python 3.4.2 (default, Oct 19 2014, 13:31:11) 
# [GCC 4.9.1] on linux
#!/usr/bin/env python
'''
Helper class. Includes everthing that is needed to control the motion sensor
Ref: https://gpiozero.readthedocs.io/en/v1.3.1/api_input.html#motion-sensor-d-sun-pir
'''
from gpiozero import MotionSensor, LED

from time import sleep
import time, configparser

from framework import Constants
from framework.Logger import Logger

class MotionHelper():

    def __init__(self):
        self.myLogger = Logger().getLogger()
        self.myLogger.debug('Initializing MotionHelper...')

        self.config = configparser.ConfigParser()
        self.config.readfp(open(Constants.FILE_CONF))

    # Blocks the thread until motion is detected and returns True when it does
    def detectMotion(self):
        self.myLogger.debug('Entering detectMotion(...)')

        motionDetected = False
        try:
            pir = MotionSensor(self.config.getint('Ports', 'motion_sensor'))
            self.myLogger.info('Motion detector initialized. Waiting for input')

            # Blocks the thread indefinitely until motion is detected
            pir.wait_for_motion()
            motionDetected = True
            self.myLogger.info('Motion detected')

        except:
            # This is fatal for this thread. Let the exception propagate
            self.myLogger.exception('Unable to process movement')

        finally:
            pir.close()

        self.myLogger.debug('Exiting detectMotion(...)')
        return motionDetected

